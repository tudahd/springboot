package com.higgsup;

import com.higgsup.domain.Journal;
import com.higgsup.repository.JournalRepositry;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootDemoApplication {

	@Bean
	InitializingBean saveData(JournalRepositry repositry) {
		return () -> {
			repositry.save(new Journal("Get to know Spring Boot","today i will learn Spring Boot","28/03/2018"));
			repositry.save(new Journal("Simple Spring Boot Project","I will do my first Spring Boot Project","28/03/2018"));
			repositry.save(new Journal("Spring Boot Reading","Read more about Spring Boot","28/03/2018"));
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemoApplication.class, args);
	}
}

package com.higgsup.repository;

import com.higgsup.domain.Journal;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Asus on 3/27/2018.
 */
public interface JournalRepositry extends JpaRepository<Journal, Long>{
}
